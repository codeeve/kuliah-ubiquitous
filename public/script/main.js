// register the service worker if available
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function(reg) {
        console.log('Your maid is ready to serve you, Master (✿˶◡‿◡˶)', reg);
    }).catch(function(err) {
        console.warn('Error whilst registering service worker (✿ ･。･)', err);
    });
}

window.addEventListener('online', function(e) {
    // re-sync data with server
    console.log("You are online");
    Page.hideOfflineWarning();
}, false);

window.addEventListener('offline', function(e) {
    // queue up events for server
    console.log("You are offline");
}, false);

// check if the user is connected
if (navigator.onLine) {

} else {
    // show offline message
    alert("Anda sedang offline");
}

